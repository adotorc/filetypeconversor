/*
 * Copyright 2013 Jordi Casas-Roma, Alexandre Dotor Casals
 *
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 *
 *       http://www.apache.org/licenses/LICENSE-2.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
*/
package org.uoc.kison.parsers;

import interviews.graphs.Graph;

import java.io.BufferedReader;
import java.io.FileReader;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import org.apache.log4j.Logger;
import org.uoc.kison.interfaces.Parser;
import org.uoc.kison.objects.SimpleIntGraph;

public class TxtParser implements Parser {

    protected static final Logger logger = Logger.getLogger(Parser.class);
    protected String parsedFile; // Content of file

    @Override
    public SimpleIntGraph parseFileToSimpleIntGraph(String filename) {
        logger.info("TxtParser is selected!");

        try {
            SimpleIntGraph graph = new SimpleIntGraph();

            BufferedReader in = new BufferedReader(new FileReader(filename));
            Pattern intsOnly = Pattern.compile("\\d+");

            while (in.ready()) {
                String line = in.readLine();
                if (!line.startsWith("#") && !line.startsWith("%")) {

                    Matcher makeMatch = intsOnly.matcher(line);

                    String source = null;
                    String target = null;

                    if (makeMatch.find()) {
                        source = makeMatch.group();
                    }
                    if (makeMatch.find()) {
                        target = makeMatch.group();
                    }

                    if (source != null && target != null) {
                        int iSource = Integer.parseInt(source);
                        int iTarget = Integer.parseInt(target);

                        while (iSource >= graph.getNumNodes()) {
                            graph.addNode();
                        }

                        graph.addEdge(iSource, iTarget);
                    }
                }
            }

            in.close();
            return graph;
        } catch (Exception e) {
            logger.error("Message: " + e.getMessage());
            e.printStackTrace();
            return null;
        }
    }

	@Override
	public Graph parseFileToInterviewGraph(String filename) {
		logger.info("TxtParser is selected!");

		int numNodes = getNumNodes(filename);
        
		try {                
            Graph graph = new Graph(numNodes);
            
            BufferedReader in = new BufferedReader(new FileReader(filename));
            Pattern intsOnly = Pattern.compile("\\d+");

            while (in.ready()) {
                String line = in.readLine();
                if (!line.startsWith("#") && !line.startsWith("%")) {

                    Matcher makeMatch = intsOnly.matcher(line);

                    String source = null;
                    String target = null;

                    if (makeMatch.find()) {
                        source = makeMatch.group();
                    }
                    if (makeMatch.find()) {
                        target = makeMatch.group();
                    }

                    if (source != null && target != null) {
                        int iSource = Integer.parseInt(source);
                        int iTarget = Integer.parseInt(target);

                        graph.addEdge(iSource, iTarget);
                    }
                }
            }
            in.close();
            
            return graph;
        } catch (Exception e) {
            logger.error("Message: " + e.getMessage());
            e.printStackTrace();
            return null;
        }
	}
	
	private int getNumNodes(String filename){
		int numNodes = 0;
        try {
            // First pass through the file to read the number of nodes
            BufferedReader in = new BufferedReader(new FileReader(filename));
            Pattern intsOnly = Pattern.compile("\\d+");

            while (in.ready()) {
                String line = in.readLine();
                if (!line.startsWith("#") && !line.startsWith("%")) {

                    Matcher makeMatch = intsOnly.matcher(line);

                    String source = null;
                    String target = null;

                    if (makeMatch.find()) {
                        source = makeMatch.group();
                    }
                    if (makeMatch.find()) {
                        target = makeMatch.group();
                    }

                    if (source != null && target != null) {
                        int iSource = Integer.parseInt(source);
                        int iTarget = Integer.parseInt(target);

                        if (iSource > numNodes) numNodes = iSource;
                        if (iTarget > numNodes) numNodes = iTarget;
                    }
                }
            }
            in.close();
            return numNodes + 1;
        } catch (Exception e) {
            logger.error("Message: " + e.getMessage());
            e.printStackTrace();
            return -1;
        }
	}
}
