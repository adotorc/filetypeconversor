/*
 * Copyright 2013 Jordi Casas-Roma, Alexandre Dotor Casals
 *
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 *
 *       http://www.apache.org/licenses/LICENSE-2.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
*/
package org.uoc.kison.parsers;

import interviews.graphs.Graph;

import java.util.HashMap;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import org.apache.log4j.Logger;
import org.uoc.kison.interfaces.Parser;
import org.uoc.kison.objects.SimpleIntGraph;
import org.uoc.kison.utils.FileUtils;

public class GmlParser implements Parser{	
	protected static final Logger logger = Logger.getLogger(Parser.class);

	protected HashMap<String, Integer> labelRelation; // Relation between new node index and it's file's ID
	protected String parsedFile; // Content of file
	

	@Override
	public SimpleIntGraph parseFileToSimpleIntGraph(String filename) {
		logger.info("GmlParser is selected!");

		parsedFile = FileUtils.readTextFile(filename);
		
		/** Check if graph is directed or indirected **/
		removeComments();
		Pattern directedPattern = Pattern.compile("directed\\s+(\\d)");
		Matcher directedMatcher = directedPattern.matcher(parsedFile);
		boolean directed = false; // default behavior
		if (directedMatcher.find()) directed = (Integer.parseInt(directedMatcher.group(1)) == 1);
		
		int numNodes = parseNodes();		
		SimpleIntGraph graph = (SimpleIntGraph)parseEdges(numNodes, directed, Parser.SIMPLE_INT_GRAPH);

		return graph;
	}
	
	@Override
	public Graph parseFileToInterviewGraph(String filename) {
		logger.info("GmlParser is selected!");

		parsedFile = FileUtils.readTextFile(filename);
		
		/** Check if graph is directed or indirected **/
		removeComments();
		Pattern directedPattern = Pattern.compile("directed\\s+(\\d)");
		Matcher directedMatcher = directedPattern.matcher(parsedFile);
		boolean directed = false; // default behavior
		if (directedMatcher.find()) directed = (Integer.parseInt(directedMatcher.group(1)) == 1);
		
		int numNodes = parseNodes();		
		Graph graph = (Graph)parseEdges(numNodes, directed, Parser.INTERVIEW_GRAPH);

		return graph;
	}

	private int parseNodes() {
		Pattern genericNodePattern = Pattern.compile("node\\s*\\[((?!\\snode\\s*\\[|\\sedge\\s*\\[).)*\\s*\\]", Pattern.DOTALL);
		Matcher matcher = genericNodePattern.matcher(parsedFile);

		Pattern nodeIdPattern = Pattern.compile("id\\s+(\\w+)");
		Matcher nodeIdMatcher = nodeIdPattern.matcher("");

		labelRelation = new HashMap<String,Integer>(nodeIdMatcher.groupCount());
		int count = 0;
		int lastMatchPosition = 0;
		while (matcher.find()) {
			nodeIdMatcher.reset(matcher.group());
			if (nodeIdMatcher.find()) {
				labelRelation.put(nodeIdMatcher.group(1),count);
				count++;
			}
			lastMatchPosition = matcher.end();
		}
		// remove nodes from string, we'll not need them in the future
		parsedFile = parsedFile.substring(lastMatchPosition, parsedFile.length());		
		logger.info(String.format("%d nodes added!", count));
		return count;
	}

	private Object parseEdges(int numNodes, boolean directed, int type) {
		Object graph = null;
		if (type == Parser.SIMPLE_INT_GRAPH) graph = new SimpleIntGraph(numNodes);
		else if (type == Parser.INTERVIEW_GRAPH) graph = new Graph(numNodes);
		else return null;

		Pattern genericEdgePattern = Pattern.compile("edge\\s*\\[((?!\\snode\\s*\\[|\\sedge\\s*\\[).)*\\s*\\]", Pattern.DOTALL);
		Matcher matcher = genericEdgePattern.matcher(parsedFile);

		Pattern edgeSourcePattern = Pattern.compile("source\\s+(\\w+)");
		Matcher edgeSourceMatcher = edgeSourcePattern.matcher("");

		Pattern edgeTargetPattern = Pattern.compile("target\\s+(\\w+)");
		Matcher edgeTargetMatcher = edgeTargetPattern.matcher("");

		int count = 0;
		while (matcher.find()) {
			edgeSourceMatcher.reset(matcher.group());
			edgeTargetMatcher.reset(matcher.group());
			if (edgeSourceMatcher.find() && edgeTargetMatcher.find()) {
				Integer iSource = labelRelation.get(edgeSourceMatcher.group(1));
				Integer iTarget = labelRelation.get(edgeTargetMatcher.group(1));
				if (iSource != null && iTarget != null){
					if (type == Parser.SIMPLE_INT_GRAPH) ((SimpleIntGraph)graph).addEdge(iSource, iTarget);
					else if (type == Parser.INTERVIEW_GRAPH) ((Graph)graph).addEdge(iSource, iTarget);
					if (!directed){
						if (type == Parser.SIMPLE_INT_GRAPH) ((SimpleIntGraph)graph).addEdge(iTarget, iSource);
						else if (type == Parser.INTERVIEW_GRAPH) ((Graph)graph).addEdge(iTarget, iSource);
						count++;
					}
					count++;
				}
			}
		}
		logger.info(String.format("%d edges added!", count));
		return graph;
	}	
	
	private int removeComments(){
		Pattern commentPattern = Pattern.compile("#.*(\\r\\n|\\n)");
		Matcher matcher = commentPattern.matcher(parsedFile);
		int count = 0;
		while (matcher.find()) {
			parsedFile = parsedFile.replace(matcher.group(), "");
			count++;
		}
		return count;
	}

}