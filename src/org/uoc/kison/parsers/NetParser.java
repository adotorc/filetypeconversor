/*
 * Copyright 2013 Jordi Casas-Roma, Alexandre Dotor Casals
 *
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 *
 *       http://www.apache.org/licenses/LICENSE-2.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
*/
package org.uoc.kison.parsers;

import interviews.graphs.Graph;

import java.util.HashMap;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import org.apache.log4j.Logger;
import org.uoc.kison.interfaces.Parser;
import org.uoc.kison.objects.SimpleIntGraph;
import org.uoc.kison.utils.FileUtils;

public class NetParser implements Parser{
	protected static final Logger logger = Logger.getLogger(Parser.class);

	protected HashMap<String,Integer> labelRelation; // Relation between new node index and it's file's ID
	protected String parsedFile; // Content of file

	@Override
	public SimpleIntGraph parseFileToSimpleIntGraph(String filename) {
		logger.info("NetParser is selected!");

		parsedFile = FileUtils.readTextFile(filename);

		removeComments();

		int numNodes = parseNodes();		
		SimpleIntGraph graph = (SimpleIntGraph)parseEdges(numNodes, Parser.SIMPLE_INT_GRAPH);

		return graph;
	}
	
	@Override
	public Graph parseFileToInterviewGraph(String filename) {
		logger.info("NetParser is selected!");

		parsedFile = FileUtils.readTextFile(filename);

		removeComments();

		int numNodes = parseNodes();		
		Graph graph = (Graph)parseEdges(numNodes, Parser.INTERVIEW_GRAPH);

		return graph;
	}

	private int parseNodes() {
		Pattern nodeSizePattern = Pattern.compile("\\s*\\*Vertices\\s+(\\d+)",Pattern.CASE_INSENSITIVE);
		Matcher nodeSizeMatcher = nodeSizePattern.matcher(parsedFile);
		nodeSizeMatcher.find();
		int nodeSize = 0;
		try{
			nodeSize = Integer.parseInt(nodeSizeMatcher.group(1));
			labelRelation = new HashMap<String,Integer>(nodeSize);
		}catch (Exception e){
			e.printStackTrace();
			return 0;
		}

		Pattern nodeListPattern = Pattern.compile("\\s*\\*vertices\\s+\\d+\\s+(((?!\\*arcs\\s+|\\*arcslist\\s+|\\*edges\\s+|\\*edgeslist\\s+).)*)", Pattern.DOTALL | Pattern.CASE_INSENSITIVE);
		Matcher nodeListMatcher = nodeListPattern.matcher(parsedFile);

		int count = 0;
		if (nodeListMatcher.find()) {
			Pattern nodeIdPattern = Pattern.compile("\\s*(\\w+).*\\s");
			Matcher nodeIdMatcher = nodeIdPattern.matcher(nodeListMatcher.group(1));
			while(nodeIdMatcher.find()){
				labelRelation.put(nodeIdMatcher.group(1),count);
				count++;
			}
		}
		// remove nodes from string, we'll not need them in the future
		parsedFile = parsedFile.substring(nodeListMatcher.end(), parsedFile.length());		
		logger.info(String.format("%d nodes added!", labelRelation.size()));
		return nodeSize;
	}

	private Object parseEdges(int numNodes, int type) {
		Object graph = null;
		if (type == Parser.SIMPLE_INT_GRAPH) graph = new SimpleIntGraph(numNodes);
		else if (type == Parser.INTERVIEW_GRAPH) graph = new Graph(numNodes);
		else return null;
		
		int numArcs = addArcs(graph); // Parse and add edges from tag Arcs (directed)
		int numEdges = addEdges(graph); // Parse and add edges from tag Edges(undirected)
		
		int numArcsList = addArcsLists(graph); // Parse and add edges from tag ArcsList (directed)
		int numEdgesList = addEdgeLists(graph); // Parse and add edges from tag EdgesList(undirected)
		
		logger.info(String.format("%d edges added! (arcs: %d, edges: %d, arcslist: %d, edgeslist: %d)", numArcs+numEdges+numArcsList+numEdgesList, numArcs, numEdges, numArcsList, numEdgesList));

		return graph;
	}


	private int addArcs(Object graph){		
		Pattern arcListPattern = Pattern.compile("\\*arcs\\s+(((?!\\*arcs\\s+|\\*arcslist\\s+|\\*edges\\s+|\\*edgeslist\\s+).)*)", Pattern.DOTALL | Pattern.CASE_INSENSITIVE);
		Matcher arcListMatcher = arcListPattern.matcher(parsedFile);
		int count = 0;
		
		SimpleIntGraph sig = null;
		Graph ig = null;
		if (graph instanceof SimpleIntGraph) sig = (SimpleIntGraph) graph;
		else if (graph instanceof Graph) ig = (Graph) graph;
		else return -1;
		

		if (arcListMatcher.find()){
			Pattern edgePattern = Pattern.compile("(\\d+)\\s+(\\d+)[ \\t\\x0B\\f]+\\d+|(\\d+)\\s+(\\d+)");
			Matcher edgeMatcher = edgePattern.matcher(arcListMatcher.group(1));
			while(edgeMatcher.find()){	
				Integer iSource = labelRelation.get(edgeMatcher.group(1));
				Integer iTarget = labelRelation.get(edgeMatcher.group(2));
				if (iSource != null && iTarget != null){
					if (sig != null) sig.addEdge(iSource, iTarget);
					else if (ig != null) ig.addEdge(iSource, iTarget);
					count++;
				}
			}
		}

		logger.debug(String.format("%d arcs added!", count));
		return count;
	}


	private int addEdges(Object graph){		
		Pattern edgesListPattern = Pattern.compile("\\*edges\\s+(((?!\\*arcs\\s+|\\*arcslist\\s+|\\*edges\\s+|\\*edgeslist\\s+).)*)", Pattern.DOTALL | Pattern.CASE_INSENSITIVE);
		Matcher edgesListMatcher = edgesListPattern.matcher(parsedFile);
		int count = 0;
		
		SimpleIntGraph sig = null;
		Graph ig = null;
		if (graph instanceof SimpleIntGraph) sig = (SimpleIntGraph) graph;
		else if (graph instanceof Graph) ig = (Graph) graph;
		else return -1;

		if (edgesListMatcher.find()){ // we have results either from arcs or edges
			Pattern edgePattern = Pattern.compile("(\\d+)\\s+(\\d+)[ \\t\\x0B\\f]+\\d+|(\\d+)\\s+(\\d+)");
			Matcher edgeMatcher = edgePattern.matcher(edgesListMatcher.group(1));
			while(edgeMatcher.find()){
				String source = edgeMatcher.group(1);
				String target = edgeMatcher.group(2);
				if (source == null && target == null){ // Second case (right part of OR)
					source = edgeMatcher.group(3);
					target = edgeMatcher.group(4);
				}
				Integer iSource = labelRelation.get(source);
				Integer iTarget = labelRelation.get(target);
				if (iSource != null && iTarget != null){
					if (sig != null){
						sig.addEdge(iSource, iTarget);
						sig.addEdge(iTarget, iSource);
					}else if (ig != null){
						ig.addEdge(iSource, iTarget);
						ig.addEdge(iTarget, iSource);
					}
					count+=2;
				}
			}
		}

		logger.debug(String.format("%d edges added!", count));
		return count;
	}


	private int addEdgeLists(Object graph){
		Pattern edgesListPattern = Pattern.compile("\\*edgeslist\\s+(((?!\\*arcs\\s+|\\*arcslist\\s+|\\*edges\\s+|\\*edgeslist\\s+).)*)", Pattern.DOTALL | Pattern.CASE_INSENSITIVE);
		Matcher edgesListMatcher = edgesListPattern.matcher(parsedFile);
		int count = 0;
		
		SimpleIntGraph sig = null;
		Graph ig = null;
		if (graph instanceof SimpleIntGraph) sig = (SimpleIntGraph) graph;
		else if (graph instanceof Graph) ig = (Graph) graph;
		else return -1;

		if (edgesListMatcher.find()){ // we have results either from arcs or edges
			Pattern edgesLinesPattern = Pattern.compile("[^\\n\\r]+[\\n\\r]");
			Matcher edgesLinesMatcher = edgesLinesPattern.matcher(edgesListMatcher.group(1));
			while(edgesLinesMatcher.find()){
				Pattern edgePattern = Pattern.compile("\\d");
				Matcher edgeMatcher = edgePattern.matcher(edgesLinesMatcher.group(0));
				boolean first = true;
				String source = "";
				while(edgeMatcher.find()){
					if (first){
						source =  edgeMatcher.group(0);
						first = false;
					}else{
						Integer iSource = labelRelation.get(source);
						Integer iTarget = labelRelation.get(edgeMatcher.group(0));
						if (iSource != null && iTarget != null){
							if (sig != null){
								sig.addEdge(iSource, iTarget);
								sig.addEdge(iTarget, iSource);
							}else if (ig != null){
								ig.addEdge(iSource, iTarget);
								ig.addEdge(iTarget, iSource);
							}
							count+=2;
						}
					}
				}
			}			
		}

		logger.debug(String.format("%d edgesList added!", count));
		return count;
	}

	
	private int addArcsLists(Object graph){
		Pattern arcsListPattern = Pattern.compile("\\*arcslist\\s+(((?!\\*arcs\\s+|\\*arcslist\\s+|\\*edges\\s+|\\*edgeslist\\s+).)*)", Pattern.DOTALL | Pattern.CASE_INSENSITIVE);
		Matcher arcsListMatcher = arcsListPattern.matcher(parsedFile);
		int count = 0;
		
		SimpleIntGraph sig = null;
		Graph ig = null;
		if (graph instanceof SimpleIntGraph) sig = (SimpleIntGraph) graph;
		else if (graph instanceof Graph) ig = (Graph) graph;
		else return -1;

		if (arcsListMatcher.find()){ // we have results either from arcs or edges
			Pattern arcsLinesPattern = Pattern.compile("[^\\n\\r]+[\\n\\r]");
			Matcher arcsLinesMatcher = arcsLinesPattern.matcher(arcsListMatcher.group(1));
			while(arcsLinesMatcher.find()){
				Pattern arcPattern = Pattern.compile("\\d");
				Matcher arcMatcher = arcPattern.matcher(arcsLinesMatcher.group(0));
				boolean first = true;
				String source = "";
				while(arcMatcher.find()){
					if (first){
						source =  arcMatcher.group(0);
						first = false;
					}else{
						Integer iSource = labelRelation.get(source);
						Integer iTarget = labelRelation.get(arcMatcher.group(0));
						if (iSource != null && iTarget != null){
							if (sig != null) sig.addEdge(iSource, iTarget);
							else if (ig != null) ig.addEdge(iSource, iTarget);
							count++;
						}
					}
				}
			}			
		}

		logger.debug(String.format("%d arcsList added!", count));
		return count;
	}
	

	private int removeComments(){
		Pattern commentPattern = Pattern.compile("%.*(\\r\\n|\\n)|\\/\\*.*\\*\\/");
		Matcher matcher = commentPattern.matcher(parsedFile);
		int count = 0;
		while (matcher.find()) {
			parsedFile = parsedFile.replace(matcher.group(), "");
			count++;
		}
		return count;
	}

}
