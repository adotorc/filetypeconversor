/*
 * Copyright 2013 Jordi Casas-Roma, Alexandre Dotor Casals
 *
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 *
 *       http://www.apache.org/licenses/LICENSE-2.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
*/
package org.uoc.kison.parsers;

import interviews.graphs.Graph;

import java.util.ArrayList;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import org.apache.log4j.Logger;
import org.uoc.kison.interfaces.Parser;
import org.uoc.kison.objects.SimpleIntGraph;
import org.uoc.kison.utils.FileUtils;

public class MetisParser implements Parser{
	protected static final Logger logger = Logger.getLogger(Parser.class);
	protected String parsedFile; // Content of file

	@Override
	public SimpleIntGraph parseFileToSimpleIntGraph(String filename) {
		logger.info("MetisParser is selected!");

		parsedFile = FileUtils.readTextFile(filename);

		SimpleIntGraph graph = (SimpleIntGraph) parseEdges(Parser.SIMPLE_INT_GRAPH);

		return graph;
	}
	
	@Override
	public Graph parseFileToInterviewGraph(String filename) {
		logger.info("MetisParser is selected!");

		parsedFile = FileUtils.readTextFile(filename);

		Graph graph = (Graph) parseEdges(Parser.INTERVIEW_GRAPH);

		return graph;
	}


	private Object parseEdges(int type) {
		parsedFile += '\n'; // to make sure there is a new line at the end of the file
		Pattern linesPattern = Pattern.compile("(.*)(\\r\\n|\\n)");
		Matcher linesMatcher = linesPattern.matcher(parsedFile);

		try{
			int count = 0;
			if(linesMatcher.find()){

				Pattern headerPattern = Pattern.compile("(\\d+)\\s*");
				Matcher headerMatcher = headerPattern.matcher(linesMatcher.group(1));

				ArrayList<Integer> firstLineData = new ArrayList<Integer>(4);
				while (headerMatcher.find()) firstLineData.add(Integer.parseInt(headerMatcher.group(1)));
				
				Object graph = null;
				if (type == Parser.SIMPLE_INT_GRAPH) graph = new SimpleIntGraph(firstLineData.get(0));
				else if (type == Parser.INTERVIEW_GRAPH) graph = new Graph(firstLineData.get(0));
				else return null;

				int source = 0;
				Pattern targetPattern = Pattern.compile("(\\d+)\\s*");
				Matcher targetMatcher;

				switch (firstLineData.size()) {
					case 2: /** Unweighted Graph (target target...) **/
						while (linesMatcher.find()) {
							targetMatcher = targetPattern.matcher(linesMatcher.group(1));
							while(targetMatcher.find()){
								if (type == Parser.SIMPLE_INT_GRAPH){
									((SimpleIntGraph)graph).addEdge(source, Integer.parseInt(targetMatcher.group(1)) - 1);
								}else if (type == Parser.INTERVIEW_GRAPH){
									((Graph)graph).addEdge(source, Integer.parseInt(targetMatcher.group(1)) - 1);
								}
								count++;
							}
							source++;
						}
						break;

					case 3: 
						/** Weighted Graph - third parameter specifies type
						 * type 001 - Weights on edges - (target weight target weight...)
						 * type 011 - Weights both on vertices and edges - (vertex-weight target weight target weight...)
						 **/  
						if (firstLineData.get(2) == 11 || firstLineData.get(2) == 1){ // allow only the defined types
							int index = 0;
							while (linesMatcher.find()) {
								targetMatcher = targetPattern.matcher(linesMatcher.group(1));
								if(firstLineData.get(2) == 11) targetMatcher.find(); // ignore the first result (vertex weight)
								while(targetMatcher.find()){
									if (index % 2 == 0){
										if (type == Parser.SIMPLE_INT_GRAPH){
											((SimpleIntGraph)graph).addEdge(source, Integer.parseInt(targetMatcher.group(1)) - 1);
										}else if (type == Parser.INTERVIEW_GRAPH){
											((Graph)graph).addEdge(source, Integer.parseInt(targetMatcher.group(1)) - 1);
										}
										count++;
									}
									index++;
								}
								source++;
							}
						}
						break;

					case 4: /** Multi-Constraint Graph (weight{4th parameter times} target target...) **/
						if (firstLineData.get(2) == 10){ // allow only the defined type
							int ignoreCount = firstLineData.get(3);
							while (linesMatcher.find()) {
								targetMatcher = targetPattern.matcher(linesMatcher.group(1));
								for(int i=0;i<ignoreCount;i++) targetMatcher.find(); //ignore the first ignoreCount values
								while(targetMatcher.find()){
									if (type == Parser.SIMPLE_INT_GRAPH){
										((SimpleIntGraph)graph).addEdge(source, Integer.parseInt(targetMatcher.group(1)) - 1);
									}else if (type == Parser.INTERVIEW_GRAPH){
										((Graph)graph).addEdge(source, Integer.parseInt(targetMatcher.group(1)) - 1);
									}
									
									count++;
								}
								source++;
							}
						}
						break;

					default:
						break;
				}

				logger.info(String.format("%d edges added!", count));
				return graph;				
			}
			return null;

		}catch (Exception e){
			e.printStackTrace();
			return null;
		}
	}

}
