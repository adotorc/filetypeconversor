/*
 * Copyright 2013 Jordi Casas-Roma, Alexandre Dotor Casals
 *
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 *
 *       http://www.apache.org/licenses/LICENSE-2.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
*/
package org.uoc.kison.exporters;

import interviews.graphs.Graph;

import java.util.ArrayList;
import java.util.HashSet;

import org.apache.log4j.Logger;
import org.uoc.kison.interfaces.Exporter;
import org.uoc.kison.objects.SimpleIntGraph;
import org.uoc.kison.utils.FileUtils;

public class MetisExporter implements Exporter{
	protected static final Logger logger = Logger.getLogger(Exporter.class);
	private static final int maxLength = 1024 * 1024; // 1 MB

	@Override
	public boolean exportToFile(SimpleIntGraph graph, String filename, boolean directed) {
		logger.info("MetisExporter is selected!");
		StringBuilder data = new StringBuilder();
		boolean result;
		boolean append = false;

		int numNodes = graph.getNumNodes();
		int numEdges = graph.getNumEdges();
		data.append(numNodes+" "+numEdges+"\n");
		
		for (int i=0;i<numNodes;i++){
			HashSet<Integer> targets = graph.getEdges(i);
			int j=0;
			for (int target : targets) {
				if (j!=0) data.append(' ');
				data.append(target+1);
				j++;
			}
			data.append('\n');
			
			if (data.length() > maxLength){
				result = FileUtils.writeFile(data.toString(), filename, append);
				if (!result) return false;
				append = true;
				data.delete(0, data.length());
			}
		}
		
		result = FileUtils.writeFile(data.toString(), filename, append);
		if (!result) return false;

		logger.debug("MetisExporter finished!");
		return true;
	}

	@Override
	public boolean exportToFile(Graph graph, String filename, boolean directed) {
		logger.info("MetisExporter is selected!");
		StringBuilder data = new StringBuilder();
		boolean result;
		boolean append = false;

		int numNodes = graph.V;
		int numEdges = graph.E();
		data.append(numNodes+" "+numEdges+"\n");
		
		for (int i=0;i<numNodes;i++){
			Iterable<Integer> targets = graph.adjV(i);
			boolean first = true;
			for (int target: targets){
				if (first) first = false;
				else data.append(' ');
				data.append(target + 1);
			}
			data.append('\n');
			
			if (data.length() > maxLength){
				result = FileUtils.writeFile(data.toString(), filename, append);
				if (!result) return false;
				append = true;
				data.delete(0, data.length());
			}
		}
		
		result = FileUtils.writeFile(data.toString(), filename, append);
		if (!result) return false;

		logger.debug("MetisExporter finished!");
		return true;
	}

}
