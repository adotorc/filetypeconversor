/*
 * Copyright 2013 Jordi Casas-Roma, Alexandre Dotor Casals
 *
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 *
 *       http://www.apache.org/licenses/LICENSE-2.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
*/
package org.uoc.kison;

import org.uoc.kison.exporters.GmlExporter;
import org.uoc.kison.exporters.MetisExporter;
import org.uoc.kison.exporters.NetExporter;
import org.uoc.kison.exporters.TxtExporter;
import org.uoc.kison.interfaces.Exporter;
import org.uoc.kison.interfaces.Parser;
import org.uoc.kison.objects.SimpleIntGraph;
import org.uoc.kison.parsers.GmlParser;
import org.uoc.kison.parsers.MetisParser;
import org.uoc.kison.parsers.NetParser;
import org.uoc.kison.parsers.TxtParser;

public class FileTypeConversor {

    public static enum FileType { GML, TXT, NET, METIS }

    public boolean convert(FileType from, String originFilename, FileType to, String destinationFilename, boolean directed) {
        /** 1- originFilename -> SimpleIntGraph (parsing) **/
        SimpleIntGraph graph = null;
        Parser parser;
        switch (from) {
            case GML:
                parser = new GmlParser();
                graph = parser.parseFileToSimpleIntGraph(originFilename);
                break;

            case TXT:
                parser = new TxtParser();
                graph = parser.parseFileToSimpleIntGraph(originFilename);
                break;

            case NET:
                parser = new NetParser();
                graph = parser.parseFileToSimpleIntGraph(originFilename);
                break;

            case METIS:
                parser = new MetisParser();
                graph = parser.parseFileToSimpleIntGraph(originFilename);
                break;
        }

        if (graph == null) {
            return false;
        }

        /** 2- SimpleIntGraph -> destinationFilename (exporting) **/
        Exporter exporter;
        boolean success = false;
        switch (to) {
            case GML:
                exporter = new GmlExporter();
                success = exporter.exportToFile(graph, destinationFilename, directed);
                break;

            case TXT:
                exporter = new TxtExporter();
                success = exporter.exportToFile(graph, destinationFilename, directed);
                break;

            case NET:
                exporter = new NetExporter();
                success = exporter.exportToFile(graph, destinationFilename, directed);
                break;

            case METIS:
                exporter = new MetisExporter();
                success = exporter.exportToFile(graph, destinationFilename, directed);
                break;
        }

        return success;
    }

    protected FileType getFileType(String extension) {
        if (extension.compareToIgnoreCase("GML") == 0) {
            return FileTypeConversor.FileType.GML;

        } else if (extension.compareToIgnoreCase("TXT") == 0) {
            return FileTypeConversor.FileType.TXT;

        } else if (extension.compareToIgnoreCase("NET") == 0) {
            return FileTypeConversor.FileType.NET;

        } else if (extension.compareToIgnoreCase("METIS") == 0) {
            return FileTypeConversor.FileType.METIS;
        }

        return null;
    }
}
