/*
 * Copyright 2013 Jordi Casas-Roma, Alexandre Dotor Casals
 *
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 *
 *       http://www.apache.org/licenses/LICENSE-2.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
*/
package org.uoc.kison.objects;

import java.util.ArrayList;
import java.util.HashSet;

public class SimpleIntGraph {

    private ArrayList<HashSet<Integer>> adjList;

    /**
     * Empty constructor, you have to call addNode to initialize the space to use a new node
     */
    public SimpleIntGraph() {
        this.adjList = new ArrayList<HashSet<Integer>>();
    }

    /**
     * Constructor that initializes numNodes nodes
     */
    public SimpleIntGraph(int numNodes) {
        this.adjList = new ArrayList<HashSet<Integer>>(numNodes);
        for (int i = 0; i < numNodes; i++) {
            adjList.add(new HashSet<Integer>());
        }
    }

    public boolean addEdge(int source, int target) {
        if (source < adjList.size()) {
            if (!adjList.get(source).contains(target)) {
                adjList.get(source).add(target);
            }
            return true;
        }
        return false;
    }

    public boolean deleteEdge(int source, int target) {
        if (source < adjList.size()) {
            return adjList.get(source).remove((Integer) target);
        }
        return false;
    }

    public HashSet<Integer> getEdges(int nodeSource) {
        if (nodeSource < adjList.size()) {
            return adjList.get(nodeSource);
        }
        return null;
    }

    public int getNumNodes() {
        return adjList.size();
    }

    public int getNumEdges() {
        int count = 0;
        for (HashSet<Integer> anAdjList : adjList) {
            count += anAdjList.size();
        }
        return count;
    }
    
    public int getDegreeMax() {
        int max = 0;
        for (HashSet<Integer> anAdjList : adjList) {
            if (anAdjList.size() > max)
                max = anAdjList.size();
        }
        return max;
    }
    
    public float getAverageDegree() {
        return getDegreeMax() / (float) getNumNodes();
    }

    /**
     * Only use this function if you used the empty constructor OR if you want more
     * nodes than the specified in the contructor(int)
     */
    public void addNode() {
        adjList.add(new HashSet<Integer>());
    }
}
