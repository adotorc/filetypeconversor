/*
 * Copyright 2013 Jordi Casas-Roma, Alexandre Dotor Casals
 *
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 *
 *       http://www.apache.org/licenses/LICENSE-2.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
*/

package org.uoc.kison;

import org.apache.commons.io.FilenameUtils;
import org.apache.log4j.Logger;
import org.apache.log4j.PropertyConfigurator;

public class Main {

    private static final Logger logger = Logger.getLogger(Main.class);
    private static final String version = "1.0.0";

    public static void main(String[] args) {
        PropertyConfigurator.configure("log4j.properties");

        if (args.length == 3) {
            String inputFileName = args[0];
            String type = args[1];
            String outputExtension = args[2];

            String inputFullPath = FilenameUtils.getFullPath(inputFileName);
            String inputBaseName = FilenameUtils.getBaseName(inputFileName);
            String inputExtension = FilenameUtils.getExtension(inputFileName);
            
            String outputFileName = inputFullPath + inputBaseName +"."+ outputExtension;

            logger.info("*************************************************");
            logger.info("* Graph FileType Conversor                      *");
            logger.info("* Jordi Casas-Roma (jcasasr@uoc.edu)            *");
            logger.info("* Alexandre Dotor Casals (adotorc@uoc.edu)      *");
            logger.info("* Universitat Oberta de Catalunya (www.uoc.edu) *");
            logger.info("*************************************************");
            logger.info("");
            logger.info(String.format("Version %s", version));
            logger.info(String.format("Input filname   : %s", inputFileName));
            logger.info(String.format("Type            : %s", type));
            logger.info(String.format("Output filename : %s", outputFileName));
            logger.info("");
            logger.info("--------------------------------------------------");
            
            boolean directed = false;
            if (type.equalsIgnoreCase("directed")) directed = true;
            else if (type.equalsIgnoreCase("undirected")) directed = false;
            
            FileTypeConversor ftc = new FileTypeConversor();
            ftc.convert(ftc.getFileType(inputExtension), inputFileName, ftc.getFileType(outputExtension), outputFileName, directed);

        } else {
            System.out.println("FileTypeConversor Version " + version);
            System.out.println("Usage: java -jar FileTypeConversor <input filename> <type> <output type>");
            System.out.println("    <type>: directed, undirected");
            System.out.println("    <output type>: GML, TXT, NET, METIS");
            System.exit(-1);
        }
    }
    
    
}
