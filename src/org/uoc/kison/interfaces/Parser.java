/*
 * Copyright 2013 Jordi Casas-Roma, Alexandre Dotor Casals
 *
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 *
 *       http://www.apache.org/licenses/LICENSE-2.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
*/
package org.uoc.kison.interfaces;

import interviews.graphs.Graph;

import org.uoc.kison.objects.SimpleIntGraph;

public interface Parser {
	
	public static final int SIMPLE_INT_GRAPH = 1;
	public static final int INTERVIEW_GRAPH = 2;
	
	
	/**
	 * Parse file and return it's graph
	 * @param filename
	 * @return SimpleIntGraph
	 */
	SimpleIntGraph parseFileToSimpleIntGraph(String filename);
	
	/**
	 * Parse file and return it's graph
	 * @param filename
	 * @return Interview Graph
	 */
	Graph parseFileToInterviewGraph(String filename);
}
