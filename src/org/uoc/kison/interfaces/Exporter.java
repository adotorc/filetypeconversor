/*
 * Copyright 2013 Jordi Casas-Roma, Alexandre Dotor Casals
 *
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 *
 *       http://www.apache.org/licenses/LICENSE-2.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
*/
package org.uoc.kison.interfaces;

import interviews.graphs.Graph;

import org.uoc.kison.objects.SimpleIntGraph;

public interface Exporter {
	/**
	 * Exports data from the graph to a file
	 * @param graph - SimpleIntGraph to write
	 * @param filename - path to the file to write
	 * @param directed - directed/undirected graph
	 * @return success on writing the file
	 */
	boolean exportToFile(SimpleIntGraph graph, String filename, boolean directed);
	
	/**
	 * Exports data from the graph to a file
	 * @param graph - Interview graph to write
	 * @param filename - path to the file to write
	 * @param directed - directed/undirected graph
	 * @return success on writing the file
	 */
	boolean exportToFile(Graph graph, String filename, boolean directed);
}
