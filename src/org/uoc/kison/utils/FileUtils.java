/*
 * Copyright 2013 Jordi Casas-Roma, Alexandre Dotor Casals
 *
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 *
 *       http://www.apache.org/licenses/LICENSE-2.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
*/
package org.uoc.kison.utils;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileWriter;
import java.io.IOException;
import java.io.InputStreamReader;

import org.apache.log4j.Logger;

public class FileUtils {
	protected static final Logger logger = Logger.getLogger(FileUtils.class);

	public static String readTextFile(String filename) {
		try {
			File file = new File(filename);
			FileInputStream fis = new FileInputStream(file);
			BufferedReader in = new BufferedReader(new InputStreamReader(fis));
			char[] text = new char[(int) file.length()];
			while (!in.ready()) {}
			in.read(text);
			in.close();

			return new String(text);

		} catch (FileNotFoundException e) {
			logger.error(e.getMessage());
			return null;
		} catch (IOException e) {
			logger.error(e.getMessage());
			return "";
		}
	}
	
	public static boolean writeFile(String data, String filename, boolean append){
	    try {
	        FileWriter writer = new FileWriter(filename, append);
	        BufferedWriter bufferedWriter = new BufferedWriter(writer, 1048576); // 1 MB (1024*1024)
	        
	        bufferedWriter.write(data);
	        bufferedWriter.flush();
	        bufferedWriter.close();
	        
	        return true;
	    }catch (Exception e){
	    	logger.error(e.getMessage());
	    	return false;
	    }
	}
}
